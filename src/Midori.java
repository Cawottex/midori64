public class Midori {
	static byte[] plaintext = { 0x4, 0x2, 0xc, 0x2, 0x0, 0xf, 0xd, 0x3, 0xb, 0x5, 0x8, 0x6, 0x8, 0x7, 0x9, 0xe };
	static byte[] plaintext2 = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 ,0x0};
	static byte[] keys = { 0x6, 0x8, 0x7, 0xd, 0xe, 0xd, 0x3, 0xb, 0x3, 0xc, 0x8, 0x5, 0xb, 0x3, 0xf, 0x3, 0x5, 0xb,
			0x1, 0x0, 0x0, 0x9, 0x8, 0x6, 0x3, 0xe, 0x2, 0xa, 0x8, 0xc, 0xb, 0xf };
	static byte[] keys2 ={ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 ,0x0,0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 ,0x0};

	static byte[][] matriceBase = { { 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1 },
			{ 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0 }, { 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1 },
			{ 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1 }, { 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1 },
			{ 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0 },
			{ 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0 }, { 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
			{ 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0 }, { 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1 },
			{ 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0 }, { 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0 } };

	static byte[] mixColonne(byte[] tableau) {
		byte[] temp = new byte[tableau.length];
		for (int i = 0; i < 4; i++) {
			temp[(i * 4)] = (byte) (tableau[(i * 4 + 1)] ^ tableau[(i * 4 + 2)] ^ tableau[(i * 4 + 3)]);
			temp[(i * 4) + 1] = (byte) (tableau[(i * 4 + 0)] ^ tableau[(i * 4 + 2)] ^ tableau[(i * 4 + 3)]);
			temp[(i * 4) + 2] = (byte) (tableau[(i * 4 + 0)] ^ tableau[(i * 4 + 1)] ^ tableau[(i * 4 + 3)]);
			temp[(i * 4) + 3] = (byte) (tableau[(i * 4 + 0)] ^ tableau[(i * 4 + 1)] ^ tableau[(i * 4 + 2)]);

		}
		return temp;
	}

	static byte[] Sbox(byte[] tableau) {
		byte[] sbox0 = { 0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6 };
		for (int i = 0; i < 16; i++) {
			tableau[i] = sbox0[tableau[i]];
		}
		return tableau;

	}

	static byte[] ShuffleCell(byte[] monTableau) {
		byte[] copie = monTableau.clone();
		//
		monTableau[0] = copie[0];
		monTableau[1] = copie[10];
		monTableau[2] = copie[5];
		monTableau[3] = copie[15];
		monTableau[4] = copie[14];
		monTableau[5] = copie[4];
		monTableau[6] = copie[11];
		monTableau[7] = copie[1];
		monTableau[8] = copie[9];
		monTableau[9] = copie[3];
		monTableau[10] = copie[12];
		monTableau[11] = copie[6];
		monTableau[12] = copie[7];
		monTableau[13] = copie[13];
		monTableau[14] = copie[2];
		monTableau[15] = copie[8];
		return monTableau;
	}

	public static void afficherC(byte[] t) {
		System.out.println("\n");
		for (int i = 0; i < t.length; i++) {

			System.out.print(t[i] + " ");
		}
		System.out.println("\n");
	}

	public static byte[] keyGenerator(byte[] key, int tour, byte[] cypher) {

		if (tour % 2 == 0) {
			for (int i = 0; i < 16; i++) {
				cypher[i] = (byte) (cypher[i] ^ key[i] ^ matriceBase[tour][i]);
			}

		} else {
			for (int i = 0; i < 16; i++) {
				cypher[i] = (byte) (cypher[i] ^ key[16 + i] ^ matriceBase[tour][i]);
			}
		}
		return cypher;
	}

	public static byte[] encrypt(byte[] plainText, byte[] key, boolean verbose) {
		byte[] cypher = plainText;
		for (int i = 0; i < 16; i++) {
			cypher[i] = (byte) (cypher[i] ^ key[i] ^ key[i + 16]);
		}
		for (int i = 0; i < 15; i++) {
			if (verbose) {
				System.out.println(" tour  = " + i);
				afficherC(cypher);
			}
			cypher = Sbox(cypher);
			if (verbose) {
				System.out.println("---------Sortie Subcell----------");
				afficherC(cypher);
			}
			cypher = ShuffleCell(cypher);
			if (verbose) {
				System.out.println("---------ShuffleCell----------");
				afficherC(cypher);
			}
			cypher = mixColonne(cypher);
			if (verbose) {
				System.out.println("---------mixColonne----------");
				afficherC(cypher);
			}
			cypher = keyGenerator(key, i, cypher);
			if (verbose) {
				System.out.println("---------keyGenerator----------");
				afficherC(cypher);
			}
		}
		cypher = Sbox(cypher);
		for (int i = 0; i < 16; i++) {
			cypher[i] = (byte) (cypher[i] ^ key[i] ^ key[i + 16]);
		}

		return cypher;
	}

	public static void main(String[] args) {

		/*
		 * Modifier le verbose a true pour afficher les étapes intermédiaires
		 */
		boolean verbose = false;
		System.out.println("pour le plainText : " );
		afficherC(plaintext);
		System.out.println("pour la cle : ");
		afficherC(keys);
		byte[] cypher = encrypt(plaintext, keys, verbose);

		System.out.println("---------CYPHER TEXT----------");
		afficherC(cypher);

		System.out.println("-------------------------------------");
		System.out.println("pour le plainText : ");
		afficherC(plaintext2);
		System.out.println("pour la cle : " );
		afficherC(keys2);

		byte[] cypher2 = encrypt(plaintext2, keys2, verbose);
		System.out.println("---------CYPHER TEXT----------");
		afficherC(cypher2);

	}

}